// EXAMPLE USAGE on pins D1 & D2
CANChannel can(CAN_D1_D2);

int numDevices = 0; //updated on self ID sequence

int led = D7;

int idOut = D3; //Id out pin

int numSlaves = 0;

//cloud func prototypes
int getOneModule(String id);
int getAllModules(String x);
int signalModule(String id);
int selfTest(String x);

void setup() {
    pinMode(idOut, OUTPUT); //init
    digitalWrite(idOut, LOW);

    pinMode(led, OUTPUT); //init
    digitalWrite(led, LOW);

    //init particle funcs
    Particle.function("getOneModule", getOneModule);
    Particle.function("getAllModules", getAllModules);
    Particle.function("signalModule", signalModule);
    Particle.function("selfTest", selfTest);

    can.begin(250000); //init can with 250kbps speed

    delay(500);

    setIDs(); //detect all nodes
}

void loop() {
    CANMessage msg;
    if (can.receive(msg)) {
        digitalWrite(led, !digitalRead(led));
    }
}

void setIDs() { //call this to set all ID's
    digitalWrite(idOut, HIGH);

    CANMessage msgOut;
    msgOut.len = 8;
    msgOut.data[0] = 1; //pairing mode code
    msgOut.id = 0; //master ID is always 0


    for (int i = 1; i<250; i++) {
        delay(10);
        msgOut.data[0] = 1;
        msgOut.data[1] = i;
        can.transmit(msgOut);

        bool exitFlag = false;
        unsigned long startMillis = millis();
        while (can.available() < 1) {
            delay(1);
            unsigned long endMillis = millis();

            //exit id mode if it takes longer than a second for node to respond (means end of chain)
            if ((long)(endMillis - startMillis) > 1000) {
                exitFlag = true;
                break;
            }

        }

        //throw out the CAN message. Very important...
        if (can.available() > 0) {
            CANMessage IDmsg;
            can.receive(IDmsg);
            if (IDmsg.id == i && IDmsg.data[0] == 1) { //data[0] ==1 signifies successful pairing message
                numDevices++;
            }
        }

        if (exitFlag == true) { //means all devices are connected, end of daisy chain reached
            Particle.publish("numDevices", String(numDevices));

            if (numDevices != 0) {
                RGB.control(true);
                for (int i = 0; i < numDevices; i++) { //blink numDevices times
                    RGB.color(255, 255, 0);
                    delay(200);
                    RGB.color(0, 0, 0);
                    delay(200);
                }
                RGB.control(false);
            } else {
                RGB.control(true);
                for (int i = 0; i < 20; i++) { //no devices connected, error
                    RGB.color(255, 0, 0);
                    delay(100);
                    RGB.color(0, 0, 0);
                    delay(100);
                }
                RGB.control(false);
            }
            break;
        }

    }
    //end for loop

}

int getOneModule(String x = "1") {
    int id = 1;
    id = x.toInt();

    if (id > numDevices) { //error
        return int(-2);
    }

    if (id < 1) { //error
        return int(-2);
    }

    CANMessage msgOut;
    msgOut.len = 8; //important!
    msgOut.id = 0;
    //turn on emitter on previous device
    msgOut.data[0] = 3;
    msgOut.data[1] = id - 1;
    can.transmit(msgOut);
    delay(5);

    //poll pins on current device
    msgOut.data[0] = 2;
    msgOut.data[1] = id; //id to respond;
    can.transmit(msgOut);

    unsigned long startMillis = millis();
    while (can.available() < 1){
        unsigned long endMillis = millis();
        if ((long)(endMillis - startMillis) > 500) {
            String errorString = "Module " + String(id) + " has disconnected!";
            Particle.publish("Hardware Error", errorString);
            break;
        }
    }

    if (can.available() > 0) {
        CANMessage msg;
        can.receive(msg);
        if (msg.id == id && msg.data[0] == 2) { //verification
            return int(msg.data[1]);
        }
    }
}

int getAllModules(String x) {
    String response = "";

    for (int i = 0; i < numDevices; i++) {
        response.concat(getOneModule(String(i + 1)));
        response.concat(" ");
        delay(150); //100
    }

    Particle.publish("Response", response);
    return 1;
}

int selfTest(String x) {
    return 1;
}

int signalModule(String x = "0") {
    int id = 0;
    id = x.toInt();

    if (id < 1 || id > numDevices) {
        return -2;
    }

    CANMessage msgOut;
    msgOut.len = 8;
    msgOut.id = 0;
    msgOut.data[0] = 4;
    msgOut.data[1] = id;
    can.transmit(msgOut);
    return 1;
}
