#include "mbed.h"
// pinout http://docs.zephyrproject.org/_images/nucleo_l432kc_arduino_nano.png
//SLAVE CODE FOR IR SENSORS

//CHANGE THESE TO SUIT EACH SLAVE
#define NUM_INPUTS 4
#define SLAVE_TYPE 1 //indicates IR slave

CAN can1(PB_8, PB_9); // CAN bus pins

AnalogIn inputs[NUM_INPUTS] = {PA_1, PA_2, PA_3, PA_4};

DigitalOut led1(PB_12); // LED (status)

// EXTRA WIRE DAISY CHAIN PINS

DigitalIn idIn(PB_10);   // identify wire IN (pin D3) (incorrect before)
DigitalOut idOut(PB_11); // identify wire OUT (pin D4) (incorrect before)


int myID = 0; // initalize ID, will change on identify

void quickFlash(void) { //quick led flash
    led1 = 1;
    wait(.01);
    led1 = 0;
}

int selfTest(void) {
    int failedInputs = 0;
    for (int i = 0; i < NUM_INPUTS; i++) {
        if (inputs[i].read() < 0.005) {
            failedInputs++;
        }
    }
    return failedInputs;
}

void selfIdentify(int id) {
    if (idIn == 1 && idOut == 0) {

        myID = id;

        CANMessage msgOut;
        msgOut.id = myID;
        msgOut.len = 8;
        msgOut.data[0] = 1;
        msgOut.data[1] = int(SLAVE_TYPE);
        msgOut.data[2] = int(NUM_INPUTS);

        // printf("%s", "My ID is: ");
        // printf("%d\n", msgOut.id);

        can1.write(msgOut);

        idOut = 1; // move on to next device in chain

        led1 = 1; //flash led
        wait(.2);
        led1 = 0;

    }
}

int main() {
    // printf("%s\n", "Powered up");
    can1.frequency(250000);
    can1.mode(CAN::Normal);
    led1 = 0;

    // init inputs with pull DOWN
    idIn.mode(PullDown);

    while (1) { // superloop
        CANMessage msg;
        // --- MESSAGE IN ---
        if (can1.read(msg)) {
            // MESSAGE FROM MASTER
            if (msg.id == 0) {
                if (msg.data[0] == 1) { // signifies pairing mode
                    selfIdentify(int(msg.data[1]));

                // DATA REQUEST ---------------------
                } else if (msg.data[0] == 2) { // signifies data request
                    if (msg.data[1] == myID) { // wants my pin info!!
                        // send out data
                        for (int i = 0; i < NUM_INPUTS; i++) {
                            wait(.001);
                            CANMessage msgOut;
                            msgOut.id = myID;
                            msgOut.len = 8;
                            msgOut.data[0] = 2;

                            unsigned short val = inputs[i].read() * 10000;
                            unsigned char highByte = (val >> 8) & 0xFF;
                            unsigned char lowByte =  val & 0xFF;

                            msgOut.data[1] = lowByte;
                            msgOut.data[2] = highByte;

                            can1.write(msgOut);
                        }

                    }

                // END DATA REQUEST -----------------------------

                } else if (msg.data[0] == 4) { // signifies signal mode
                    if (msg.data[1] == myID) {
                        for (int i = 0; i < 40; i++) {
                            led1 = !led1;
                            wait(.2);
                        }
                    }

                } else if (msg.data[0] == 5) { // signifies self test sequence
                    if (msg.data[1] == myID) {
                        led1 = !led1;
                        CANMessage msgOut;
                        msgOut.id = myID;
                        msgOut.len = 8;
                        msgOut.data[0] = 5;

                        if (selfTest() > 0) {
                            msgOut.data[1] = 0; // 0 is a failed test
                            can1.write(msgOut);
                        } else {
                            msgOut.data[1] = 1; // 1 is a passed test
                            can1.write(msgOut);
                        }
                        quickFlash();
                    }
                    //
                }
            }
        } // end of if incoming message
    } // end super loop
} // end main loop
